#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define SIZE 10	// Cantidad de elementos en secuencia de números reales.
#define MAX 5	// Valor máximo que puede tomar cada elemento.

// Prototipos
void iniNums(float numList[], unsigned int n, unsigned int maxNum);
void impNums(const float mumList[], unsigned int n);
float promedio(const float nums[], unsigned int n);
float desviacion(const float nums[], unsigned int n);
void limpieza(const float nums[], unsigned int n, float coe);

int main(void){
	float numeros[SIZE];
	float coeficiente;

	srand(time(NULL));

	iniNums(numeros, SIZE, MAX);
	impNums(numeros, SIZE);

	printf("Promedio: %6.2f\n", promedio(numeros, SIZE));
	printf("Desviación estándar: %6.2f\n", desviacion(numeros, SIZE));

	printf("%s","Ingresa coeficiente: " );
	scanf("%f", &coeficiente);

	limpieza(numeros, SIZE, coeficiente);

	return 0;
}

// Inicializa arreglo numList[] de tamaño n con números flotantes entre 0 y maxNum.
void iniNums(float numList[], unsigned int n, unsigned int maxNum){ 
	for (size_t i = 0; i < n; i++){
		numList[i] = ((float)rand() / (float)(RAND_MAX)) * maxNum; 
	} 
}

// Imprime arreglo numList[] de tamaño n.
void impNums(const float numList[], unsigned int n){
	printf("%s\n|", "Secuencia de números reales:");
	for (size_t i = 0; i < n; i++){
		printf("%6.2f|", numList[i]);
	}
	puts("");
}

/* ### Función: promedio
 * Suma cada uno de los elementos del arreglo y divide el total por el tamaño del
 * arreglo.
 * #### Entradas
 * - nums: arreglo de números reales.
 * - n: tamaño de arreglo nums.
 * #### Salidas
 * - promedio de los valores del arreglo nums. */
float promedio(const float nums[], unsigned int n){
	float suma = 0.0;
	for (size_t i = 0; i < n; i++){
		suma += nums[i];
	}
	return suma / n;
}

/* ### Función: desviacion
 * Suma la resta al cuadrado entre cada elemento del arreglo y el promedio, luego
 * divide por el tamaño del arreglo y aplica raíz cuadrada.
 * #### Entradas
 * - numbs: arreglo de números reales.
 * - n: tamaño de arreglo.
 * #### Salidas
 * - Desviación estandar de los valores del arreglo. */
float desviacion(const float nums[], unsigned int n){
	float prom;
	float sumatoria = 0;

	prom = promedio(nums, n);

	for (size_t i = 0; i < n; i++){
		sumatoria += powf(nums[i] - prom, 2);
	}

	return sqrtf(sumatoria / n);
}

/* ### Función: limpieza
 * Imprime en pantalla la limpieza de un arreglo de números reales.
 * #### Entradas
 * - nums: arreglo de números reales.
 * - n: tamaño de arreglo.
 * - coe: Coeficiente para rango.
 * #### Salidas
 * Ninguna. */
void limpieza(const float nums[], unsigned int n, float coe){
	float prom;
	float desv;

	prom = promedio(nums, n);
	desv = desviacion(nums, n);

	printf("%s\n|", "Limpieza:");

	for (size_t i = 0; i < n; i++){
		if (nums[i] >= prom - (coe * desv) && nums[i] <= prom + (coe *
					desv)){
			printf("%6.2f|", nums[i]);

		}

	}

	puts("");

}
