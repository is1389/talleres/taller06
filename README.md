# Caracterización y limpieza de una muestra

Una secuencia (arreglo) de $`n`$ números reales (_float_) se puede caracterizar
y limpiar usando el promedio y la desviación estándar. El promedio de una
secuencia de números se define como:

```math
\mu = \frac{1}{n} \sum_{i = 0}^{n - 1}x_i
```

y la desviación estándar se puede definir como:

```math
\sigma = \sqrt{\frac{1}{n} \sum_{i = 0}^{n - 1}(x_i - \mu)^2}
```

La limpieza de la secuencia de números se hace informando cuáles de ellos
tienen valores en el rango ( $`\mu - \alpha \cdot \sigma`$,  $`\mu + \alpha
\cdot \sigma`$) , donde $`\alpha`$ es un número dado por el usuario.

1. Proponga una función que calcule el promedio de un arreglo de números
   reales. Esta función no debe imprimir nada por pantalla ni pedir entradas
   por el teclado.

1. Proponga una función que calcule la desviación estándar de un arreglo de
   números reales. Esta función no debe imprimir nada por pantalla ni pedir
   entradas por el teclado

   > **Nota:** la función `float sqrt(float a)` calcula la raíz
cuadrada de un número.

1. Proponga una función que imprima por pantalla la limpieza de un arreglo de
   números reales. Esta función no debe pedir entradas por el teclado.

# Solución propuesta

## Refinamiento 1

### Función: `promedio`

Suma cada uno de los elementos del arreglo y divide el total por el tamaño del
arreglo.

#### Entradas

- Arreglo de números reales.
- Tamaño de arreglo.

#### Salidas

- Promedio de los valores del arreglo.

#### Pseudo-código

1. Recorro arreglo:
   1. Sumo elemento
1. Divido total de suma en tamaño y retorno resultado.

### Función: `desviacion`

Suma la resta al cuadrado entre cada elemento del arreglo y el promedio, luego
divide por el tamaño del arreglo y aplica raíz cuadrada.

#### Entradas

- Arreglo de números reales.
- Tamaño de arreglo.

#### Salidas

- Desviación estandar de los valores del arreglo.

#### Pseudo-código

1. Obtengo promedio del arreglo.
1. Recorro arreglo:
   1. Suma la resta al cuadrado entre el elemento y el promedio.
1. Divido total de suma en tamaño, aplico raíz cuadrada y retorno resultado.

### Función: `limpieza`

Imprime en pantalla la limpieza de un arreglo de números reales.

#### Entradas

- Arreglo de números reales.
- Tamaño de arreglo.
- Coeficiente para rango.

#### Entradas

Ninguna.

#### Pseudo-código

1. Recorro arreglo:
   1. Si elemento está entre rango:
      1. Imprimo elemento.

## Refinamiento 2

### Función: `promedio`

Suma cada uno de los elementos del arreglo y divide el total por el tamaño del
arreglo.

#### Entradas

- `nums`: arreglo de números reales.
- `n`: tamaño de arreglo `nums`.

#### Salidas

- promedio de los valores del arreglo `nums`.

#### Pseudo-código

1. Declaro variable `suma` y asigno valor de `0`.
1. Desde `i = 0`; hasta  `i < n`; incremento `i` en `1`:
   1. Sumo elemento `nums[i]` a `suma`
1. Divido total de `suma` en tamaño `n` y retorno resultado.

### Función: `desviacion`

Suma la resta al cuadrado entre cada elemento del arreglo y el promedio, luego
divide por el tamaño del arreglo y aplica raíz cuadrada.

#### Entradas

- `nums`: arreglo de números reales.
- `n`: tamaño de arreglo.

#### Salidas

- Desviación estandar de los valores del arreglo.

#### Pseudo-código

1. Declaro variable `prom`.
1. Declaro variable `sumatoria`.
1. Invoco función `promedio()` con `nums` y `n` como parámetro, asigno retorno
   a `prom`.
1. Desde `i = 0`; hasta  `i < n`; incremento `i` en `1`:
   1. Adiciono a `sumatoria` la resta al cuadrado entre `nums[i]` y `prom`.
1. Divido `sumatoria` en tamaño `n`, aplico raíz cuadrada y retorno resultado.

### Función: `limpieza`

Imprime en pantalla la limpieza de un arreglo de números reales.

#### Entradas

- `nums`: arreglo de números reales.
- `n`: tamaño de arreglo.
- `coe`: Coeficiente para rango.

#### Salidas

Ninguna.

#### Pseudo-código

1. Declaro variable `prom`.
1. Declaro variable `desv`.
1. Invoco función `promedio()` con `nums` y `n` como parámetro, asigno retorno
   a `prom`.
1. Invoco función `desviacion()` con `nums` y `n` como parámetro, asigno
   retorno a `desv`.
1. Desde `i = 0`; hasta  `i < n`; incremento `i` en `1`:
   1. Si `nums[i]` es mayor o igual a `prom - (coe * desv)` y `nums[i]` es
      menor o igual a `prom + (coe * desv)`
      1. Imprimo `nums[i]`.
